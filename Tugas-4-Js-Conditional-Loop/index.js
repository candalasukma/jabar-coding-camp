//Soal 1
var nilai = 45;
//Jawaban Soal 1
if (nilai >= 85) {
  console.log("indeksnya A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("indeksnya B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("indeksnya C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("indeksnya D");
} else if (nilai < 55) {
  console.log("indeksnya E");
}

//Soal 2
var tanggal = 21;
var bulan = 5;
var tahun = 1995;
//Jawaban Soal 2
switch (bulan) {
  case 5:
    {
      console.log(tanggal + " Mei " + tahun);
    }
    break;
}

//Soal 3
//Jawaban Soal 3

//Untuk n=3
var segitiga = "";
for (var tinggi = 0; tinggi < 3; tinggi++) {
  for (var alas = 0; alas <= tinggi; alas++) {
    segitiga += "#";
  }
  segitiga += "\n";
}
console.log(segitiga);
//Untuk n=7
var segitiga = "";
for (var tinggi = 0; tinggi < 7; tinggi++) {
  for (var alas = 0; alas <= tinggi; alas++) {
    segitiga += "#";
  }
  segitiga += "\n";
}
console.log(segitiga);

//Soal 4
//Jawaban Soal no.4
var stProgram = "I Love programming";
var stJava = "I Love Javascript";
var stVue = "I Love VueJs";
var m = 3;
var index = 1;

for (var i = 1; i <= m; i++) {
  if (index == 1) console.log(i + " - " + stProgram);
  else if (index == 2) console.log(i + " - " + stJava);
  else {
    console.log(i + " - " + stVue);
    console.log("=".repeat(i));
  }
  if (index == 3) index = 0;
  index += 1;
}
