//Soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//jawaban soal 1
var per1 = pertama.substr(0, 4);
var per2 = pertama.substr(12, 6);

var perGabung = per1 + " " + per2;

var ked1 = kedua.substr(0, 7);
var ked2 = kedua.substr(8, 10);
var ked2Up = ked2.toUpperCase();

var kedGabung = ked1 + " " + ked2Up;

console.log(perGabung.concat(" " + kedGabung));

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//jawaban soal 2
var st1 = parseInt(kataPertama);
var st2 = parseInt(kataKedua);
var st3 = parseInt(kataKetiga);
var st4 = parseInt(kataKeempat);

console.log(st1, st2, st3, st4);
console.log(st1 + st2 * st3 + st4);

//Soal 3
var kalimat = "wah javascript itu keren sekali";

var kata1 = kalimat.substring(0, 3);
var kata2 = kalimat.substring(4, 14);
var kata3 = kalimat.substring(15, 18);
var kata4 = kalimat.substring(19, 24);
var kata5 = kalimat.substring(25);

console.log("Kata Pertama: " + kata1);
console.log("Kata Kedua: " + kata2);
console.log("Kata Ketiga: " + kata3);
console.log("Kata Keempat: " + kata4);
console.log("Kata Kelima: " + kata5);
